package xkcd_viewer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ComicDatabase {
	
	public static final String DB_FILENAME = "database.txt";
	public static final String XKCD_URL = "http://xkcd.com/";
	public static final String XKCD_ARCHIVE = "archive";
	
//	private String mDbPath;
	
	private int mCurComicId;
	
	private ArrayList<Comic> mComics;

	public ComicDatabase(String path) {
//		mDbPath = path;
		mComics = new ArrayList<Comic>();
		
		update();
	}
	
	private void update() {
		URL url = null;
		InputStreamReader sr = null;
		StringBuilder sb = new StringBuilder();
		String line = null;
		String list;
		
		try {
			url = new URL(XKCD_URL+XKCD_ARCHIVE);
			sr = new InputStreamReader(url.openConnection().getInputStream());
			
			BufferedReader bufferedReader = new BufferedReader(sr);
			
			while((line = bufferedReader.readLine()) != null) {
				sb.append(line) ;
				sb.append("\n");
			}
			
			bufferedReader.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Pattern p = Pattern.compile("<div id=\"middleContainer\" class=\"box\">(.*?)</div>", Pattern.DOTALL);
		Matcher m = p.matcher(sb);
		if (m.find()) {
			list = m.group(1);
		} else {
			return;
		}
		
		p = Pattern.compile("<a href=\"/(.*?)/\" title=\"(.*?)\">(.*?)</a><br/>", Pattern.DOTALL);
		m = p.matcher(list);
		while (m.find()) {
			mComics.add(new Comic(m.group(1), m.group(3), m.group(2)));
		}
		
		Collections.reverse(mComics);
	}
	
	public Comic getPreviousComic() {
		if (mCurComicId > 0) {
			mCurComicId--;
			Comic curComic = mComics.get(mCurComicId);
			return getComic(curComic);
		}
		
		return null;
	}
	
	public Comic getNextComic() {
		if (mCurComicId < mComics.size()-1) {
			mCurComicId++;
			Comic curComic = mComics.get(mCurComicId);
			return getComic(curComic);
		}
		
		return null;
	}
	
	public Comic getRandomComic() {
		Random rand = new Random();
		int low = 0;
		int high = mComics.size();
		int id = rand.nextInt(high-low) + low;
		
		mCurComicId = id;
		Comic curComic = mComics.get(mCurComicId);
		
		return getComic(curComic);
	}
	
	public Comic getSearchedComic(String query) {
		Comic target = null;
		
		for (int i = 0; i < mComics.size(); i++) {
			if (mComics.get(i).getTitle().toLowerCase().equals(query.toLowerCase())) {
				mCurComicId = i;
				target = getComic(mComics.get(mCurComicId));
				break;
			}
		}
		
		return target;
	}
	
	private Comic getComic(Comic comic) {
		System.out.println("Downloading comic: \"" + comic.getTitle() + "\"");
		ImageFetcher image = new ImageFetcher(comic.getLink());
		comic.setPath(image.getImage());
		return comic;
	}
	
	public static ComicDatabase create() throws IOException {
		File db = new File(DB_FILENAME);
		
		// Not yet implemented
		if (!db.isFile() && !db.createNewFile())
	    {
	        throw new IOException("Error creating database file: " + db.getAbsolutePath());
	    }
		
		return new ComicDatabase(db.getAbsolutePath());
	}
}
