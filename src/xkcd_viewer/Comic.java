package xkcd_viewer;

public class Comic {
	
	private String mId;
	private String mTitle;
	private String mDate;
	private String mLink;
	private String mPath = null;
	
	public Comic(String id, String title, String date) {
		mId = id;
		mTitle = title;
		mDate = date;
		mLink = ComicDatabase.XKCD_URL + id;
	}
	
	public void setPath(String path) { mPath = path; }
	
	public String getId() { return mId; }
	public String getTitle() { return mTitle; }
	public String getDate() { return mDate; }
	public String getLink() { return mLink; }
	public String getPath() { return mPath; }
}