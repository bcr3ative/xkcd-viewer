package xkcd_viewer;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Application {
	
	private JFrame mFrame;
	
    private JPanel mControlPanel;
    
	private JButton mPreviousBtn;
	private JButton mRandomBtn;
	private JButton mNextBtn;
    private JButton mSearchBtn;
	
    private JLabel mComicViewer;
    
    private JTextField mSearchField;
    
    private ComicDatabase mDatabase;
    
    private static final String APP_TITLE = "xkcd Viewer";

    public static void main(String[] args) {
        new Application();
    }

    public Application() {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
                	e.printStackTrace();
                }

                mFrame = new JFrame(APP_TITLE);
                
                mComicViewer = new JLabel();
                mComicViewer.setHorizontalAlignment(JLabel.CENTER);
                mComicViewer.setVerticalAlignment(JLabel.CENTER);
                
                try {
        			mDatabase = ComicDatabase.create();
        		} catch (IOException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
                
                mControlPanel = new JPanel();

                mPreviousBtn = new JButton("< Previous");
                mPreviousBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                    	Comic comic = mDatabase.getPreviousComic();
                    	
                    	if (comic != null) {
                    		try {
    							mComicViewer.setIcon(new ImageIcon(ImageIO.read(new File(comic.getPath()))));
    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}
                            mFrame.setTitle(APP_TITLE + " - #" + comic.getId() + " - " + comic.getTitle());
                    	}
                    }
                });
                
                mRandomBtn = new JButton("Random");
                mRandomBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                    	Comic comic = mDatabase.getRandomComic();
                        try {
							mComicViewer.setIcon(new ImageIcon(ImageIO.read(new File(comic.getPath()))));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
                        mFrame.setTitle(APP_TITLE + " - #" + comic.getId() + " - " + comic.getTitle());
                    }
                });
                
                mNextBtn = new JButton("Next >");
                mNextBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                    	Comic comic = mDatabase.getNextComic();
                    	
                    	if (comic != null) {
                    		try {
    							mComicViewer.setIcon(new ImageIcon(ImageIO.read(new File(comic.getPath()))));
    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}
                            mFrame.setTitle(APP_TITLE + " - #" + comic.getId() + " - " + comic.getTitle());
                    	}
                    }
                });
                
                mSearchField = new JTextField();
                mSearchBtn = new JButton("Search");
                mSearchBtn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                    	Comic comic = mDatabase.getSearchedComic(mSearchField.getText());
                    	
                    	if (comic != null) {
                    		try {
    							mComicViewer.setIcon(new ImageIcon(ImageIO.read(new File(comic.getPath()))));
    						} catch (IOException e1) {
    							e1.printStackTrace();
    						}
                            mFrame.setTitle(APP_TITLE + " - #" + comic.getId() + " - " + comic.getTitle());
                    	}
                    }
                });

                mFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                mFrame.setLayout(new BorderLayout());
                mFrame.add(mComicViewer);
                mFrame.add(mControlPanel, BorderLayout.SOUTH);
                mControlPanel.setLayout(new GridLayout(2, 3));
                mControlPanel.add(mPreviousBtn);
                mControlPanel.add(mRandomBtn);
                mControlPanel.add(mNextBtn);
                mControlPanel.add(mSearchField);
                mControlPanel.add(mSearchBtn);
                mControlPanel.add(new JLabel(""));
                
                mRandomBtn.doClick();
                
                mFrame.pack();
                mFrame.setLocationRelativeTo(null);
                mFrame.setVisible(true);
            }
        });
    }

}