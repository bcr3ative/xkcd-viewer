package xkcd_viewer;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageFetcher {

	private String mComicUrl;
	
	public ImageFetcher(String url) {
		mComicUrl = url;
	}
	
	public String getImage() {
		URL url = null;
		InputStreamReader sr = null;
		StringBuilder sb = new StringBuilder();
		String line = null;
		String imageUrl = null;
		String imageExt = null;
		
		try {
			url = new URL(mComicUrl);
			sr = new InputStreamReader(url.openConnection().getInputStream());
			
			BufferedReader bufferedReader = new BufferedReader(sr);
			
			while((line = bufferedReader.readLine()) != null) {
				sb.append(line) ;
				sb.append("\n");
			}
			
			bufferedReader.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Pattern p = Pattern.compile("<div id=\"comic\">.*?<img src=\"(.*?)\".*?</div>", Pattern.DOTALL);
		Matcher m = p.matcher(sb);
		if (m.find()) {
			String data = m.group(1);
			imageUrl = "http:" + data;
			imageExt = data.substring(data.length() - 4);
		} else {
			return "";
		}
		
		java.net.URLConnection urlConnection;
		try {
			
			url = new URL(imageUrl);
			urlConnection = url.openConnection();
			urlConnection.setRequestProperty("http.agent", "");
			InputStream inputStream = urlConnection.getInputStream();
			OutputStream os = new FileOutputStream("pic" + imageExt);

			byte[] b = new byte[2048];
			int length;

			while ((length = inputStream.read(b)) != -1) {
				os.write(b, 0, length);
			}

			inputStream.close();
			os.close();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return "pic" + imageExt;
	}
}
